#TextProBarV3.py

import time # 导入 time 模块

scale = 50  # 设置进度条的长度为 50 个字符
print("执行开始".center(scale//2,'-'))  # 在屏幕中央打印“执行开始”和一条横线
start = time.perf_counter()  # 记录程序开始的时间
for i in range(scale+1): # 循环从 0 到 50
    a = '*' * i  # 进度条中已完成的部分用 * 表示
    b = '.' * (scale - i)  # 进度条中未完成的部分用 . 表示
    c = (i/scale)*100  # 计算进度百分比
    dur = time.perf_counter() - start  # 计算程序运行的时间
    print("\r{:^3.0f}%[{}->{}]{:.2f}s".format(c,a,b,dur),end='')  # 在同一行打印进度条和其他信息
    time.sleep(0.1)  # 每次循环暂停 0.1 秒，制造动画效果
print("\n"+"执行结束".center(scale//2,'-'))  # 在屏幕中央打印“执行结束”和一条横线