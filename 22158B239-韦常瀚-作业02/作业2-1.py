USD_VS_RMB = 6.77  # 美元兑人民币汇率

currency_str_value = input('请输入带单位的货币金额:')  # 输入带货币单位的金额

unit = currency_str_value[-3:]  # 获取货币单位

if unit == 'CNY':  # 如果货币单位是人民币
    rmb_str_value = currency_str_value[:-3]  # 获取人民币金额字符串
    rmb_value = eval(rmb_str_value)  # 将人民币字符串转换为数字
    usd_value = rmb_value / USD_VS_RMB  # 将人民币转换为美元

    print('美元(USD)金额是:', usd_value)  # 输出美元金额

elif unit == 'USD':  # 如果货币单位是美元
    usd_str_value = currency_str_value[:-3]  # 获取美元金额字符串
    usd_value = eval(usd_str_value)  # 将美元字符串转换为数字
    rmb_value = usd_value * USD_VS_RMB  # 将美元转换为人民币

    print('人民币（CNY）金额是：', rmb_value)  # 输出人民币金额

else:  # 如果货币单位不是人民币或美元
    print('目前版本尚不支持该种货币:')  # 提示用户不支持该种货币


